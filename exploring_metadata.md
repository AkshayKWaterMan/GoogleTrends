#Metadata about search trends of COVID-19, Delta and Omicron

**The following is a graph comparing the search trends of Covid-19, Delta variant, and omicron over the past 90 days:**

![image](Covid search trends.png)

**Here are some things that we noticed while looking at this graph:**
1. Searches for covid are also roughly 20 times more common than searches for omicron and delta. This could be because not as many people know about omicron and delta.
2. Searches for Covid appear to have a downward spike around every three to four days, before quickly spiking back up.
3. Another potential reason COVID is being searched more than omicron and delta is that COVID is generally a broad term that can also relate to delta and omicron. To inquire News about the variants, people must just be searching up COVID-19, instead of specifying the variant.
4. There is also the idea that by the time Delta/Omicron came around, people were too sick of COVID to care about its variants.

**For the Code.org data set, we chose the "Planets of our solar system" data set. Here are some observations we made about its metatdata:**

1. The metadata talks about different attributes of each planet, such as its name, diameter, distance from the sun, or orbital period (how long it takes to orbit the sun). A picture is also included.	
2. Each attribute also comes with a data type(string, numerical, etc.)
3. It’s very specific— some of these things are closely related with one another, and yet they are specified.

