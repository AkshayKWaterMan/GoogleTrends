# Ten new vocab words

## 1. Public Key Encryption
**Public Key Encryption** is a style of cryptography that uses a pair of keys. One is the public key, which is known to
everyone, and the other is the private key, which is known to only the owner of the key. In the method, the sender of a
message can encrypt the message using a public key in such a way that only a certain private key can decrypt it. This
method allows for privacy, compactness, and ease for both parties involved, even when more users are added.

## 2. Symmetric Key Encryption
**Symmetric Key Encryption** is the predecessor to public key encryption. While public key uses a PAIR of keys,
symmetric key uses the same key for all sides sending messages privately. The key stays as a shared secret among the
users in conversation. The problem with this, however, is that because each group of users has to have a unique key,
there are just too many keys and connections as more and more users are added. This is why Public Key encryption was
introduced.

*albert.io example:*

**Which of the following statements is most accurate concerning encryption?**
1. #### Public key encryption uses a public key for encryption and a private ey for decryption, while symmetric key encryption only involves one key for encryption and decryption. - correct answer
2. Public key encryption uses a public key for encryption and a private key for decryption, while symmetric key encryption only involves one key for both encryption and decryption.
3. Public key encryption uses one key for both encryption and decryption, while symmetric key encryption uses a public key for encryption and a private key for decryption.
4. Public key encryption uses one key for both encryption and decryption, while symmetric key encryption uses a private key for encryption and a public key for decryption.

## 3. Multi-factor authentication
**Multi-factor authentication** is when a user is identified in 2 forms, usually choosing from this list:
- Something they are
- Something they know
- Something they have
The reason 2 forms of encryption are used is to make it tougher for a hacker to obtain all the info to get into the
user's account.

*Example:*

Which of the following is **NOT** an example of multifactor authentication?
1. #### A bank customer inserts their bank smart card into the Automatic Teller Machine (ATM) and enters a Personal Identification Number (PIN) in order to withdraw some cash. - correct answer
2. A cell phone user enters a four-digit Personal Identification Number (PIN) and scans their fingerprint in order to unlock their phone.
3. A company employee enters their password and a 3-digit Personal Identification Number (PIN) in order to access the corporate website.
4. An airline traveler has their face scanned and shows their passport in order to pass through the airport’s security checkpoint.

## 4. Sequential solution
When a problem is solved by completing each task individually one after the other.

## 5. Parallel solution
When a problem is solved by completing some tasks at the same time to help speed up the process.

To better understand the difference between Sequential and Parallel solutions, check out the image below:
![image](Images/SvS_problem.png)

## 6. Lossy data compression
When a file is being sent between 2 users, sometimes the size has to be modified to help with the speed of the
transmission. This is where compression algorithms come in handy, and Lossy data compression is one of them. In lossy
data compression, the data is altered such that some parts of it are excluded. In a compressed image, some of the pixels
are removed. This increases the speed of transmission of the image, but decreases its quality. It also means that the
original version of the image with all of the pixels cannot be retrieved again.

## 7. Lossless data compression
Lossless data compression is just like lossy written above, except none of the data is removed, just altered a little.
The original image is returned, but transmissions speed is slower.

## 8. Crowd sourcing
Crowd sourcing is a technique used by businesses to consult the public before making an application. It ensures that
users get what they need and want with the application.

## 9. Fault tolerance
When a client is connecting to a server, it isn't one dedicated connection along one pathway. Instead, it is like 2
people talking on either sides of a web. Why is this? Because it is **fault tolerant**. If it was one dedicated
connection and that connection went wrong, then the 2 sides just can't communicate anymore. But with a web, there are
numerous different ways that packets can travel across the internet, so even if one breaks, many others can take over.

## 10. Rogue access point
A rogue access point is an access point(a device that sits in the place of a router or Ethernet and projects network
connection everywhere in an area) that is installed on a network without the owner's permission.
