# Definition of an Undecidable Problem

**Question:** Which of the following statements are true about an undecidable problem?


**I.** There is no algorithm that would solve all cases of an undecidable problem.
**II.** An undecidable problem may have cases where an algorithm could solve the problem.
**III.** All cases of an undecidable problem can be solved with an algorithm but it would take a very long time to solve them.

*What is an **undecidable problem**?*

According to [Brilliant's article on undecidable problems](https://brilliant.org/wiki/halting-problem/), "An **undecidable problem** is one that should give a "yes" or "no" answer, but yet no algorithm exists that can answer correctly on all inputs."
Basically, there isn't an algorithm that can be constructed to to answer an undecidable problem correctly in all cases. Some of the cases in an undecidable problem may be solvable, but the problem itself can never be fully decidable. 
One of the most famous undecidable problems is the **HALTING PROBLEM**, a decision problem in computability theory.
To understand this concept, take a look at the following program:

![Endless program](Images/endlessProg.png)

This program first takes user input and assigns it to the variable `x`. Then, it starts a loop: as long as `x` has a
real value, the loop continues. Since the value of `x` is never changed, the loop will go on infinitely. This program is
partially decidable. If x is assigned the value of null, then it will stop immediately after checking the condition for
the first time.

When programmers write applications, they always want to make sure they don't write programs like this, or at least they
want to restrict certain inputs from the user. Otherwise, the program will crash due to memory overload, and it might
even take the device it's running on with it.
To negate this problem, suppose that a fictional company creates a special function called `stopChecker` that takes a
program as input, then outputs whether that program will iterate forever or halt at some point. When the program above
is inputted into this hypothetical function, the return value should be false, as the program never stops.the following
is a flowchart for the `stopChecker` function:

![function flowchart](Images/stopChecker_flowChart.png)
***NOTE:*** `stopChecker` doesn't *run* the program given to it. Instead, it analyzes its structure.

This seems simple enough. However, such a function is logically impossible. To understand why, we introduce a new
function called `reverser`. `reverser` takes a program as input, then inputs that program for BOTH the parameters of
`stopChecker`. Basically, it finds out if a program halts when it is passed as a parameter to itself. If it does halt,
then `reverser` puts it in an infinite loop. If it iterates endlessly, however, `reverser` stops it. The following is a code representation of this hypothetical function:

![reverser function code](Images/reverser_code.png)

Things get freaky when we pass `reverser` to itself, and that is the basis for this whole problem.
Let's say that when `reverser` is passed to itself, `stopChecker` predicts that `reverser` will return "stops". This possibility would play out like so:

![reverserInReverser_true](Images/reverserInReverser_true.png)

But wait a second! do you see the problem with this? `stopChecker` predicted that `reverser` would stop, but instead it went ahead and continued. `stopChecker` was wrong!
The same applies if `stopChecker` predicts that `reverser` would go on:

![reverserInReverser_false](Images/reverserInReverser_false.png)

`stopChecker` predicted that `reverser` would loop endlessly, but instead it stopped. Both cases lead to `stopChecker` being wrong.

However, this doesn't imply that `stopChecker` is written wrong. In fact, it actually proves that the *idea* behaind `stopChecker` is logically impossible. A function like `stopChecker` can't actually exist, and even if it did, it would only work sometimes. 
**The problem that `stopChecker` was written to solve is an UNDECIDABLE PROBLEM!**

This explanation brings us back to the original problem. Which statements are correct? 
**Statement 1 is correct**, because as shown in the example, an undecidable problem can't be solved for all cases by any algorithm.
**Statement 2 is correct**, because as shown in the example, `stopChecker` was able to solve for a few cases of the problem it was meant for.
**Statement 3 is incorrect**, because in the example, `stopChecker`'s output either led the program to iterate endlessly OR stopped immediately. Neither resulted in the solving of the problem.

Therefore, the final answer is that **STATEMENTS 1 & 2 ONLY ARE CORRECT**.

Additional source: [Khan Academy: Undecidable Problems](https://www.khanacademy.org/computing/ap-computer-science-principles/algorithms-101/solving-hard-problems/a/undecidable-problems)
