# AP Final Exam test corrections
## Question 1: Least likely benefit of the New Toy system

NEW TOY SYSTEM:
![new toy system image](Images/newToySystem.png)

OLD TOY SYSTEM:
![old toy system image](Images/oldToySystem.png)

**Question(based off the image):** Which of the following is the ==LEAST== likely to be a benefit of the new system over
the old system?

**My answer:** The possibility of ordering out of stock toys decreases

My answer was incorrect because while both systems involve the customer knowing if the toy is out of stock or not, they
do so in different ways. The original system does so via e-mail, but the new system shows the stock right after the user
    clicks on the toy. This method is far faster and simpler, so it increases the chance that the customer will order
    something in stock. Therefore, it is a significant benefit.

**Correct answer:** The customer will have more time to pay for the order.

This is the least likely benefit, because the purchasing process is never mentioned for either method. Odds are that
this isn't even a benefit in the first place. Whether it be cash or credit, the assumption is that both systems use the
same way of cash collection. Therefore, this is the LEAST likely benefit and the correct answer.

---

## Question 2: How to protect data in new toy system

*This question uses the same images as question one.*

**Question:** Of the following, which is the LEAST likely to help protect data stored in the upgraded toy system?

**My answer:** The toy company could require the customer to enter their password and also answer a security question in
order to access their system.

My answer was incorrect. Asking the customer to answer a security question along with regular login info is known as
multi-factor authentication, which is actually a very secure and popular method of data storage. It seems that at the
time of taking the test, I missed the word **LEAST**!

**Correct Answer:** The customer could create a login ID using their work e-mail address instead of their personal
e-mail address.

This is the least likely among the choices to actually make an impact on security, because a hacker could find either
e-mail address as easily. A strong password is much more helpful.

---

## Question 3: Personally identifiable Info

*This question doesn't use any images.*

**Question:** Which of the following is NOT considered personally identifiable information?

**My answer:** Schools attended.

My answer was incorrect. If a person knew the schools you attended, they could look at the records to find out more
about you.

**Correct answer:** Cell phone model

This makes sense. Thousands of the same cell phone model are made everyday, and even if someone knew yours, they would
have to find you among thousands of people. It seems that my problem with this question was overthinking. Your cell
phone model alone isn't anywhere near enough to PERSONALLY identify you.

---

## Question 4: Lossy Data Compression appropriateness

*An example of Lossy data compression:*

"`Football players are always ready to play`" &rarr; "`Ftbll plyrs re alwys rdy t ply`"

When handling images, lossy data compression removes some of the pixels. It greatly reduces the size of the file,,
increasing the speed of transmission in the process. However, because some of the pixels are removed, there is a lower image resolution
and the original image cannot be retrieved. This is why it is called *lossy* data compression. It literally causes loss
via compression.

**Question:** In which of the following scenarios would it be most appropriate to use a lossy data compression
algorithm to compress and transmit an image?

**My answer:** The file size of the image is small, transmission speed is not a factor, and the recipient would prefer
that the image be of similar quality as the original image.

This answer was incorrect. If the file size of the image is already small, there is no point in doing something as
extreme as removing pixels like in lossy data compression. The same goes for transmission speed and wanting the original
quality of the image. If the recipient wanted slow, accurate transmission for a small file, *lossless data* compression is
actually perfect. I got this wrong because I didn't know what lossy data compression was at the time of test taking.

**Correct answer:** The file size of the image is large, transmission speed is important, and the recipient will 
accept lower image resolution than the original image.

In this scenario, lossy data compression would remove pixels from the large image to make it smaller. This decrease
s the image resolution, but the recipient doesn't mind lower quality. What it also does is increases the 
transmission speed, which is something the recipient definitely cares about. Therefore, lossy data compression is perfect in this scenario.

---

## Question 5: Multi-factor Authentication Examples

*What is Multi-factor authentication?*

Multi-factor authentication involves using at least 2 forms of identification, usually picked from this list of 3:

- Something you know
- Something you have
- Something you are

**Some examples of multi-factor authentication include:** 

1. At an airport security checkpoint, an airline travellers have their face scanned(something they ***ARE***) and their
passport checked(something they ***HAVE***) to pass through.
2. A cell phone user enters a 4-digit PIN(something they ***KNOW***) and scans their fingerprint(something they
***ARE***) in order to unlock their cell phone.

**Question:** Which of the following is NOT an example of multi-factor authentication?

**My answer:** A bank customer inserts their bank smart card into the automatic teller machine (ATM) and enters a
personal identification number (PIN) in order to withdraw some cash

This answer is incorrect. The bank ATM uses 2 forms of identification for a customer to access it and withdraw some cash. Customers first insert their bank card(something they ***HAVE***), then enter a PIN (something they ***KNOW***) to gain access. Therefore, this is multi-factor authentication. I got this question wrong because I didn't know the exact meaning of multi-factor authentication.

**Correct answer:** A company employee enters their password and a 3-digit PIN in order to access the corporate website.

This answer isn't multi-factor authentication. The employee enters 2 things that they know, which are a password and a
PIN. It doesn't use anything that they are or they have in their possession, therefore it is basically *SINGLE*-factor
authentication.

---

## Question 6: Rogue Access Point Definition

*What is a rogue access point?*

An **ACCESS POINT** is a device that connects to a wired router, switch, or hub via an Ethernet cable, and projects a
Wi-Fi signal to a designated area. For example, let's say you wanted internet access in your living room, but you
couldn't place an Ethernet cable there. To get internet connection, you would place an **access** point in your
bedroom, where you CAN place a cable. The device would connect via Ethernet, then project that Wi-Fi to your living
room.

The following image shows an access point:
![access point](Images/accessPoint.jpeg)

On the other hand, a ***ROGUE ACCESS POINT*** is an access point installed on a network without the explicit permission
of the network's owner.
A rogue access point can be done with good or bad intentions. For example, a company employee can install an access
point without his boss's permission to give easy internet access to others in his office. This is a good thing. On the
other hand, a hacker at a coffee shop could also install an access point without anyone knowing, then intercept the info
that anyone unlucky enough to join the network enters in.

The following is an example of a network from a rogue access point:
![rogue access point network](Images/rogueAccessPoint.png)

In the image above, the client is searching for a network to connect to at a coffee shop. You would probably notice that the network
"Coffee Shop Wifi" has a duplicate. A few weeks later, the coffee shop posts a flyer warning the customers that the
network "Coffee Shop Wifi" is a rogue access point. This means that it wasn't installed by the coffee shop, but by a
hacker trying to wrongly read and edit other's information.

**Question:** Which of the following is NOT completely true about a rogue access point?

**My answer:** Company employees may install a rogue access point to give other employees easy access to the company's
network. 

This answer was incorrect. As said in the explanation above, a rogue access point can involve an employee giving easy
access to his colleagues. Therefore, this IS possible. At the time of test taking, I didn't know what a rogue access
point was.

**Correct answer:** The intent of a rogue access point is for either an attacker or hacker to gain access to the network

As stated above, a rogue access point can be a good thing. It doesn't have to be for an attacker or hacker. Therefore,
this choice is false.

---
